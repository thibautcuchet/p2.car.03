
/**
 * Décrivez votre classe PRNGtest ici.
 *
 * @author (Wafflard, Evrard, Cuchet)
 * @version (V1.5)
 */
public class PRNG
{

    private int seed;
    private int number;
    private int max;

    public PRNG(int seed, int max){
      this.seed = seed;
      this.number = seed;
      this.max = max;
    }


    public PRNG(int max){
       this.seed = (int)(System.nanoTime()%250);
         this.max = max;
         this.number = seed;
    }

    public int getSeed(){
        return seed;
    }

    public void setSeed(int seed){
        this.seed = seed;
        this.number = seed;

    }

    public void setMax(int max){
      this.max = max;
    }

    public int getMax(){
      return this.max;
    }

    public int nextInt(){
        this.number=((this.number*1664525)+1013904223)%677;
        //String d =""+ this.number;
        //this.number =(10*Integer.parseInt(""+d.charAt(1))+Integer.parseInt(""+d.charAt(2))+Integer.parseInt(""+d.charAt(3)));

        //System.out.println(a%10);
        return (this.number%this.max);
    }

    public int nextInt(int max){
      this.number=((this.number*1664525)+1013904223)%677;
      //this.number=(this.number+1)*1593;
      //String d =""+ this.number;
      //this.number =(10*Integer.parseInt(""+d.charAt(1))+Integer.parseInt(""+d.charAt(2)));

      //System.out.println(a%10);
      return (this.number%max);
    }
}
