
/**
 * Write a description of class PRNGTest here.
 *
 * @author (Wafflard, Evrard, Cuchet)
 * @version (V1.5)
 */

public class PRNGTest
{

    private static int max = 10;
    private static int nombreGeneration = 100;

    public static void main(String[] args){
	     PRNG prng;

       //Attribution de certaines valeurs au demarage du programme
	     System.out.println("Quel valeur maximum voulez genere ?( de 0 au nombre entré)");
       max = TextIO.getlnInt()+1;

	     System.out.println("Combien de fois voulez vous generer des nombres aleatoires ?");
	     nombreGeneration = TextIO.getlnInt();


	     System.out.println("Voulez vous choisir une seed ? (oui : 1, non : 0)");
	     if(TextIO.getlnInt() == 0){
        	prng = new PRNG(max);
	     }
	     else{
		      System.out.println("Entrez la seed souhaitez");
		      prng = new PRNG(TextIO.getlnInt(),max);
	     }
       System.out.println();


       //Initialisation de variable utile au programme
        boolean testerror =false;
        int number;
        int prec = 0;
        int tableValeur[] = new int[max];
        int tablePair[][] = new int[max][max];
        int nombrePaire = 0;

        System.out.println("======================================================================================================");
        System.out.println(" Résultat des tests pour la seed : " + prng.getSeed() + " avec " + nombreGeneration + " nombres generes");
        if(nombreGeneration < 500){
          System.out.println("! La quantite de nombre genere est trop faible pour etre sur que chaque paire soit genere ");
        }
        System.out.println("======================================================================================================");

        //Debut stockage des valeurs dans deux tableaux
        for(int i = 0; i < nombreGeneration; i++){
            number = prng.nextInt(max);
            if(i != 0){
              tablePair[number][prec]++;
            }
            prec = number;
            if(!isInRange(number)) testerror = true;
            else tableValeur[number]++;
        }//Fin du sockages

        //Test si tous les nombres sont dans le domaine de generation
        if( testerror == true) System.out.println(" Attention une erreur est présente dans le domaine de generation");
        else System.out.println("   Tous les nombres generes sont bien compris dans le domaine.");
        //Fin du test

        //Test si tous les nombres ont bien ete genere
        if(!allNumberGenerated(tableValeur)) System.out.println("   Attention erreur, il y a des nombres non genere");
        else System.out.println("   Tous les nombres entre 0 et " + (max - 1)  + " sont bien generes.");
        //Fin du test

        //Test si tous les nombres sont genere de facon plus ou moins egal
        if(!allNumbreEquals(tableValeur)) System.out.println("  Attention le PRNG genere des nombres beaucoup plus souvent que d'autres");
        else System.out.println(" Tous les nombres sont generes equitablement");
        //Fin du test

        //Test si toutes les paires sont genere
        nombrePaire = allPairGenerated(tablePair);
        if( nombrePaire<=25){
          System.out.println("  Attention votre generateur ne génère pas beaucoup de paires differentes(" + nombrePaire + ")");
        }
        else if(nombrePaire>25 && nombrePaire<=75){
          System.out.println("  Attention votre generateur genere un certains nombres de paires mais ce n'est pas suffisant(" + nombrePaire + ")");
        }
        else if(nombrePaire>75 && nombrePaire<= 90){
          System.out.println("  Votre générateur genere un grand nombre de paires differentes("+nombrePaire+")");
        }
        else{
          System.out.println("  Votre generateur genere un maximum de paires differentes("+nombrePaire+")");
        }
        //Fin du test
        System.out.println("======================================================================================================");

        //Affichage des deux tableaux de valeurs
        System.out.println();
        afficheTable(tableValeur);
          System.out.println("======================================================================================================");
      
        affichePair(tablePair);
        //Fin affichage tableaux
        System.out.println("======================================================================================================");
    }


    public static boolean isInRange(int number){
        if( number < 0 || number >= max) return false;
        return true;
    }

    public static boolean allNumberGenerated(int table[]){
        for(int i = 0; i < table.length; i++){
            if(table[i] == 0) return false;
        }
        return true;
    }

    public static boolean allNumbreEquals(int table[]){
      for(int i = 0; i < table.length; i++){
        if(table[i] > Math.ceil((nombreGeneration/max)*1.15) ) return false;
      }
      return true;
    }

    public static int allPairGenerated(int table[][]){
      int count = 0;
      for(int i = 0; i < table.length;i++){
        for(int j = 0; j < table[i].length;j++){
          if(table[i][j] != 0) count++;
        }
      }
      return count;
    }

    public static void afficheTable(int table[]){
      System.out.println("Voici votre tableau de frequence");
	for(int i = 0; i < table.length; i++){
		System.out.printf(" | %5d " , i);
	}
      System.out.println(" |");
	System.out.print(" |");
	for(int i = 0; i < max*9-1; i++){
	      System.out.print("=");
	}
	System.out.println("|");
      for(int i = 0; i < table.length; i++){
        System.out.printf(" | %5d ", table[i]);
      }
      System.out.println(" |");
    }

    public static void affichePair(int table[][]){
      System.out.println("Voici votre tableau des paires");
	System.out.printf(" | %-5s ||", "");
	for(int i = 0; i < table.length; i++){
		System.out.printf("  %5d |", i);
	}
	System.out.println();
      	System.out.print(" |");
	for(int i = 0; i < max*9+8; i++){
		System.out.print("=");
	}
	System.out.println("|");
	for(int i = 0; i < table.length;i++){
        System.out.printf(" | %-5s ||", ""+i);
        for(int j = 0; j < table[i].length; j++){
          System.out.printf("  %5d |", table[i][j]);
        }
        System.out.println();
      }

    }
}
